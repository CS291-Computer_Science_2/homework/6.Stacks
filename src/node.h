#ifndef istack_H
#define istack_H
struct node {
	int data;
	node *link;
};

class istack {
	public:
		istack();
		~istack();
		void push(int n);
		void print();
		int pop();
	private:
		node *top;
};

#endif
