#include <iostream>
#include "node.h"
using namespace std;

istack::istack() {
	top= NULL;
}

istack::~istack() {
	if(top==NULL) return;
	node *tmp;
	while(top!=NULL) {
		tmp= top;
		top= top->link;
		delete tmp;
	}
}

void istack::push(int n) {
	node *tmp;
	tmp = new node;
	if(tmp==NULL) printf("\n\t\tStack is full.\n");

	tmp->data= n;
	tmp->link= top;
	top= tmp;
}

int istack::pop() {
	if(top==NULL) {
		printf("\n\t\tStack is empty.\n");
		return 0;
	}
	node *tmp;
	int n;
	tmp= top;
	n= tmp->data;
	top= top->link;
	delete tmp;
	return n;
}

void istack::print() {
cout<<" Stack Values: "<<endl;
	node *temp = top;
	while(top) {
		cout<<"\t\t"<<pop()<<endl;
		temp=temp->link;
	}
}
