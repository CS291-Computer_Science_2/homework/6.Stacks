#include <iostream>
#include "node.h"
using namespace std;

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
	istack *stk= new istack();
	stk->push(0);
	stk->push(1);
	stk->push(2);
	stk->push(3);
	stk->push(4);
	stk->push(5);
	stk->push(6);
	stk->push(7);
	stk->push(8);
	stk->push(9);
	stk->push(10);

	stk->print();

	return 0;
}
